import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { AuthService } from '../../core';
import { AlertService, FormErrorHandler, errorMessages } from '../../shared';

@Component({
  selector: 'app-password-recover',
  templateUrl: './password-recover.component.html',
  styleUrls: ['./password-recover.component.scss']
})
export class PasswordRecoverComponent implements OnInit {
  public faTimes = faTimes;
  public passwordRecoverForm: FormGroup;
  public error = errorMessages.user;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private alertService: AlertService
  ) {}

  public ngOnInit() {
    this.passwordRecoverForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  public get email() {
    return this.passwordRecoverForm.get('email');
  }

  public onPasswordRecover() {
    if (!FormErrorHandler.validateForm(this.passwordRecoverForm)) {
      return;
    }

    this.authService.passwordRecover(this.passwordRecoverForm.value).subscribe(
      message => {
        this.alertService.success(message);
      },
      error => {
        if (error instanceof Object) {
          FormErrorHandler.errorHandler(this.passwordRecoverForm, error);
        } else {
          this.alertService.error(error);
        }
      }
    );
  }
}
