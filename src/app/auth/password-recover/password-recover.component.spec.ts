import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { AbstractControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CoreModule } from '../../core';
import { SharedModule } from '../../shared';
import { PasswordRecoverComponent } from './password-recover.component';

describe('PasswordRecoverComponent', () => {
  let passwordRecoverComponent: PasswordRecoverComponent;
  let fixture: ComponentFixture<PasswordRecoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PasswordRecoverComponent],
      imports: [
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        FontAwesomeModule,
        CoreModule,
        SharedModule
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(PasswordRecoverComponent);
        passwordRecoverComponent = fixture.componentInstance;
        passwordRecoverComponent.ngOnInit();
      });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(passwordRecoverComponent).toBeTruthy();
  });

  describe('#form', () => {
    let nativeElement: any;
    let emailControl: AbstractControl;
    let errors = {};

    beforeEach(() => {
      emailControl = passwordRecoverComponent.passwordRecoverForm.controls['email'];
    });

    it('should be invalid when empty', () => {
      expect(passwordRecoverComponent.passwordRecoverForm.valid).toBeFalsy();
    });

    describe('#closeLink', () => {
      it('should render a link "x" to close the form', () => {
        nativeElement = fixture.debugElement.query(By.css('a[id=close]')).nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should navigate to sign in', fakeAsync(() => {
        const href = nativeElement.getAttribute('href');
        expect(href).toEqual('/auth/sign-in');
      }));
    });

    describe('#emailInput', () => {
      it('should render input field for email', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=email]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be invalid when empty', () => {
        expect(emailControl.valid).toBeFalsy();
      });

      it('should be required', () => {
        errors = emailControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });

      it('should be email', () => {
        emailControl.setValue('test');
        errors = emailControl.errors || {};
        expect(errors['email']).toBeTruthy();
      });
    });

    describe('#passwordRecoverButton', () => {
      it('should render button for submit form', () => {
        nativeElement = fixture.debugElement.query(By.css('button[type=submit]')).nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should call onPasswordRecover() method', () => {
        nativeElement = fixture.debugElement.query(By.css('form'));

        jest.spyOn(passwordRecoverComponent, 'onPasswordRecover');
        nativeElement.triggerEventHandler('submit', null);
        expect(passwordRecoverComponent.onPasswordRecover).toHaveBeenCalled();
      });

      it('should not send email if form is invalid', () => {
        emailControl.setValue('test');

        passwordRecoverComponent.onPasswordRecover();
        expect(passwordRecoverComponent.onPasswordRecover()).toBe(undefined);
      });
    });
  });
});
