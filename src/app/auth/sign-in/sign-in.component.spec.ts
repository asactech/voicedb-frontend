import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { AbstractControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CoreModule } from '../../core';
import { SharedModule } from '../../shared';
import { SignInComponent } from './sign-in.component';

describe('SignInComponent', () => {
  let signInComponent: SignInComponent;
  let fixture: ComponentFixture<SignInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SignInComponent],
      imports: [
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        FontAwesomeModule,
        CoreModule,
        SharedModule
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(SignInComponent);
        signInComponent = fixture.componentInstance;
      });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(signInComponent).toBeTruthy();
  });

  describe('#signInForm', () => {
    let nativeElement: any;
    let emailControl: AbstractControl;
    let passwordControl: AbstractControl;
    let rememberMeCheckboxControl: AbstractControl;
    let errors = {};

    beforeEach(() => {
      emailControl = signInComponent.signInForm.controls['email'];
      passwordControl = signInComponent.signInForm.controls['password'];
      rememberMeCheckboxControl = signInComponent.signInForm.controls['rememberMe'];
    });

    it('should be invalid when empty', () => {
      expect(signInComponent.signInForm.valid).toBeFalsy();
    });

    describe('#emailInput', () => {
      it('should render input field for email', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=email]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be invalid when empty', () => {
        expect(emailControl.valid).toBeFalsy();
      });

      it('should be required', () => {
        errors = emailControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });

      it('should be email', () => {
        emailControl.setValue('test');
        errors = emailControl.errors || {};
        expect(errors['email']).toBeTruthy();
      });
    });

    describe('#passwordInput', () => {
      it('should render input field for password', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=password]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be invalid when empty', () => {
        expect(passwordControl.valid).toBeFalsy();
      });

      it('should be required', () => {
        errors = passwordControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });

      it('should be a valid password', () => {
        passwordControl.setValue('1234');
        errors = passwordControl.errors || {};
        expect(errors['minlength']).toBeTruthy();
      });
    });

    describe('#rememberMeCheckbox', () => {
      it('should render checbox for remember me', () => {
        nativeElement = fixture.debugElement.query(By.css('input[type=checkbox]')).nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should render label "Remember Me" for checkbox', () => {
        nativeElement = fixture.debugElement.query(By.css('label')).nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be unchecked', () => {
        expect(rememberMeCheckboxControl.value).toEqual(false);
      });
    });

    describe('#signInButton', () => {
      it('should render button for submit form', () => {
        nativeElement = fixture.debugElement.query(By.css('button[type=submit]')).nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should call onSignIn() method', () => {
        nativeElement = fixture.debugElement.query(By.css('form'));

        jest.spyOn(signInComponent, 'onSignIn');
        nativeElement.triggerEventHandler('submit', null);
        expect(signInComponent.onSignIn).toHaveBeenCalled();
      });

      it('should not sign in if form is invalid', () => {
        emailControl.setValue('test');
        passwordControl.setValue('1234');

        signInComponent.onSignIn();
        expect(signInComponent.onSignIn()).toBe(undefined);
      });
    });

    describe('#recoveryLink', () => {
      beforeEach(() => {
        nativeElement = fixture.debugElement.query(By.css('a[id=recovery]')).nativeElement;
      });

      it('should render a link to recover password', () => {
        expect(nativeElement).toBeTruthy();
      });

      it('should navigate to recovery', fakeAsync(() => {
        const href = nativeElement.getAttribute('href');
        expect(href).toEqual('/auth/recovery');
      }));
    });

    describe('#singUpLink', () => {
      beforeEach(() => {
        nativeElement = fixture.debugElement.query(By.css('a[id=sign-up]')).nativeElement;
      });

      it('should render a link to sign up', () => {
        expect(nativeElement).toBeTruthy();
      });

      it('should navigate to sign up', fakeAsync(() => {
        const href = nativeElement.getAttribute('href');
        expect(href).toEqual('/auth/sign-up');
      }));
    });
  });
});
