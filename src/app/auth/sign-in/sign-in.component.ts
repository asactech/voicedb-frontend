import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

import { AuthService } from "../../core";
import { AlertService, FormErrorHandler, errorMessages } from "../../shared";

@Component({
  selector: "app-sign-in",
  templateUrl: "./sign-in.component.html",
  styleUrls: ["./sign-in.component.scss"]
})
export class SignInComponent implements OnInit {
  public signInForm!: FormGroup;
  public faTimes = faTimes;
  public returnUrl: string;
  public error = errorMessages.user;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private alertService: AlertService
  ) {}

  public ngOnInit() {
    this.returnUrl =
      this.activatedRoute.snapshot.queryParams["returnUrl"] || "/";

    this.signInForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(6)]],
      rememberMe: [false]
    });
  }

  public get email() {
    return this.signInForm.get("email");
  }

  public get password() {
    return this.signInForm.get("password");
  }

  public get rememberMe() {
    return this.signInForm.get("rememberMe");
  }

  public onSignIn() {
    if (!FormErrorHandler.validateForm(this.signInForm)) {
      return;
    }

    this.authService
      .signIn(this.signInForm.value, this.rememberMe.value)
      .subscribe(
        () => {
          this.router.navigate([this.returnUrl]);
        },
        error => {
          if (error instanceof Object) {
            FormErrorHandler.errorHandler(this.signInForm, error);
          } else {
            this.alertService.error(error);
          }
        }
      );
  }
}
