import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { AbstractControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CoreModule } from '../../core';
import { SharedModule } from '../../shared';
import { PasswordResetComponent } from './password-reset.component';

describe('PasswordResetComponent', () => {
  let passwordResetComponent: PasswordResetComponent;
  let fixture: ComponentFixture<PasswordResetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PasswordResetComponent],
      imports: [
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        FontAwesomeModule,
        CoreModule,
        SharedModule
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({
                id: 'ABC123',
                token: 'ABCDE12345'
              })
            }
          }
        }
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(PasswordResetComponent);
        passwordResetComponent = fixture.componentInstance;
      });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(passwordResetComponent).toBeTruthy();
  });

  describe('#passwordResetForm', () => {
    let nativeElement: any;
    let passwordControl, passwordConfirmationControl: AbstractControl;
    let errors = {};

    beforeEach(() => {
      passwordControl = passwordResetComponent.passwordResetForm.controls['password'];
      passwordConfirmationControl = passwordResetComponent.passwordResetForm.controls['confirmPassword'];
    });

    it('should be invalid when empty', () => {
      expect(passwordResetComponent.passwordResetForm.valid).toBeFalsy();
    });

    describe('#closeLink', () => {
      it('should render a link "x" to close the form', () => {
        nativeElement = fixture.debugElement.query(By.css('a[id=close]')).nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should navigate to sign in', fakeAsync(() => {
        const href = nativeElement.getAttribute('href');
        expect(href).toEqual('/auth/sign-in');
      }));
    });

    describe('#passwordInput', () => {
      it('should render input field for password', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=password]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be invalid when empty', () => {
        expect(passwordControl.valid).toBeFalsy();
      });

      it('should be required', () => {
        errors = passwordControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });

      it('should be a valid password', () => {
        passwordControl.setValue('1234');
        errors = passwordControl.errors || {};
        expect(errors['minlength']).toBeTruthy();
      });
    });

    describe('#passwordConfirmationInput', () => {
      it('should render input field for confirm password', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=confirmPassword]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be required', () => {
        errors = passwordConfirmationControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });

      it('should be equal than password', () => {
        passwordControl.setValue('123456');
        passwordConfirmationControl.setValue('159487');
        errors = passwordConfirmationControl.errors || {};
        expect(errors['confirmpassword']).toBeTruthy();
      });
    });

    describe('#resetButton', () => {
      it('should render button for submit form', () => {
        nativeElement = fixture.debugElement.query(By.css('button[type=submit]')).nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should call onPasswordReset() method', () => {
        nativeElement = fixture.debugElement.query(By.css('form'));

        jest.spyOn(passwordResetComponent, 'onPasswordReset');
        nativeElement.triggerEventHandler('submit', null);
        expect(passwordResetComponent.onPasswordReset).toHaveBeenCalled();
      });

      it('should not reset if form is invalid', () => {
        passwordControl.setValue('1234');
        passwordConfirmationControl.setValue('15948');

        passwordResetComponent.onPasswordReset();
        expect(passwordResetComponent.onPasswordReset()).toBe(undefined);
      });
    });
  });
});
