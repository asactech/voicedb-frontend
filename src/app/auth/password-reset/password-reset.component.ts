import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { AuthService } from '../../core';
import { AlertService, ConfirmPasswordValidator, FormErrorHandler, errorMessages } from '../../shared';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {
  public faTimes = faTimes;
  public passwordResetForm: FormGroup;
  public error = errorMessages.user;

  private userID: string;
  private userToken: string;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService
  ) {
    this.userID = this.activatedRoute.snapshot.paramMap.get('id');
    this.userToken = this.activatedRoute.snapshot.paramMap.get('token');
  }

  public ngOnInit() {
    if (this.userToken) {
      const [expiresAt, token] = this.userToken.split('-');

      if (!expiresAt || !token) {
        this.alertService.error('Token invalido.');
      }
    }

    this.passwordResetForm = this.formBuilder.group(
      {
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', Validators.required]
      },
      {
        validator: ConfirmPasswordValidator.MatchPassword
      }
    );
  }

  public get password() {
    return this.passwordResetForm.get('password');
  }

  public get confirmPassword() {
    return this.passwordResetForm.get('confirmPassword');
  }

  public onPasswordReset() {
    if (!FormErrorHandler.validateForm(this.passwordResetForm)) {
      return;
    }

    this.authService
      .passwordReset(
        this.userID,
        this.userToken,
        this.passwordResetForm.value
      )
      .subscribe(
        message => {
          this.alertService.success(message);
          setTimeout(() => {
            this.router.navigate(['/auth/sign-in']);
          }, 4000);
        },
        error => {
          if (error instanceof Object) {
            Object.keys(error).map(key => {
              if (key === 'id' || key === 'token') {
                this.alertService.error(error[key]);
              }
            });

            FormErrorHandler.errorHandler(this.passwordResetForm, error);
          } else {
            this.alertService.error(error);
          }
        }
      );
  }
}
