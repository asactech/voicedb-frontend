import { Location } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { SharedModule } from '../shared';
import { routes } from './auth-routing.module';
import { AuthComponent } from './auth.component';
import { PasswordRecoverComponent } from './password-recover/password-recover.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';

describe('AuthComponent', () => {
  let authComponent: AuthComponent;
  let fixture: ComponentFixture<AuthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AuthComponent,
        SignInComponent,
        SignUpComponent,
        PasswordRecoverComponent,
        PasswordResetComponent
      ],
      imports: [
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes(routes),
        FontAwesomeModule,
        SharedModule,
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(AuthComponent);
        authComponent = fixture.componentInstance;
      });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(authComponent).toBeTruthy();
  });

  describe('#routing', () => {
    let location: Location;
    let router: Router;

    beforeEach(() => {
      router = TestBed.get(Router);
      location = TestBed.get(Location);
    });

    it('should navigate to /auth/sign-in', fakeAsync(() => {
      router.navigate(['/auth/sign-in']).then(() => {
        tick(16);
        expect(location.path()).toBe('/auth/sign-in');
      });
    }));

    it('should navigate to /auth/sign-up', fakeAsync(() => {
      router.navigate(['/auth/sign-up']).then(() => {
        tick(16);
        expect(location.path()).toBe('/auth/sign-up');
      });
    }));

    it('should navigate to /auth/recovery', fakeAsync(() => {
      router.navigate(['/auth/recovery']).then(() => {
        tick(16);
        expect(location.path()).toBe('/auth/recovery');
      });
    }));

    it('should navigate to /auth/reset/:id/:token', fakeAsync(() => {
      router.navigate(['/auth/reset/1234/to-ken']).then(() => {
        tick(16);
        expect(location.path()).toBe('/auth/reset/1234/to-ken');
      });
    }));
  });
});
