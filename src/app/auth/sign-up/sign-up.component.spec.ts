import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { AbstractControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CoreModule } from '../../core';
import { SharedModule } from '../../shared';
import { SignUpComponent } from './sign-up.component';

describe('SignUpComponent', () => {
  let signUpComponent: SignUpComponent;
  let fixture: ComponentFixture<SignUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SignUpComponent],
      imports: [
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        FontAwesomeModule,
        CoreModule,
        SharedModule
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(SignUpComponent);
        signUpComponent = fixture.componentInstance;
      });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(signUpComponent).toBeTruthy();
  });

  describe('#signUpForm', () => {
    let nativeElement: any;
    let emailControl: AbstractControl;
    let passwordControl: AbstractControl;
    let passwordConfirmationControl: AbstractControl;
    let acceptTermsControl: AbstractControl;
    let errors = {};

    beforeEach(() => {
      emailControl = signUpComponent.signUpForm.controls['email'];
      passwordControl = signUpComponent.signUpForm.controls['password'];
      passwordConfirmationControl = signUpComponent.signUpForm.controls['confirmPassword'];
      acceptTermsControl = signUpComponent.signUpForm.controls['accepTerms'];
    });

    it('should be invalid when empty', () => {
      expect(signUpComponent.signUpForm.valid).toBeFalsy();
    });

    describe('#closeLink', () => {
      it('should render a link "x" to close the form', () => {
        nativeElement = fixture.debugElement.query(By.css('a[id=close]')).nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should navigate to sign in', fakeAsync(() => {
        const href = nativeElement.getAttribute('href');
        expect(href).toEqual('/auth/sign-in');
      }));
    });

    describe('#emailInput', () => {
      it('should render input field for email', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=email]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be invalid when empty', () => {
        expect(emailControl.valid).toBeFalsy();
      });

      it('should be required', () => {
        errors = emailControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });

      it('should be email', () => {
        emailControl.setValue('test');
        errors = emailControl.errors || {};
        expect(errors['email']).toBeTruthy();
      });
    });

    describe('#passwordInput', () => {
      it('should render input field for password', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=password]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be invalid when empty', () => {
        expect(passwordControl.valid).toBeFalsy();
      });

      it('should be required', () => {
        errors = passwordControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });

      it('should be a valid password', () => {
        passwordControl.setValue('1234');
        errors = passwordControl.errors || {};
        expect(errors['minlength']).toBeTruthy();
      });
    });

    describe('#passwordConfirmationInput', () => {
      it('should render input field for password', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=password]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be invalid when empty', () => {
        expect(passwordConfirmationControl.valid).toBeFalsy();
      });

      it('should be required', () => {
        errors = passwordConfirmationControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });

      it('should be equal than password', () => {
        passwordControl.setValue('123456');
        passwordConfirmationControl.setValue('159487');
        errors = passwordConfirmationControl.errors || {};
        expect(errors['confirmpassword']).toBeTruthy();
      });
    });

    describe('#acceptTermsCheckbox', () => {
      it('should render checbox for accept terms', () => {
        nativeElement = fixture.debugElement.query(By.css('input[type=checkbox]')).nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should render label "Accept terms..." for checkbox', () => {
        nativeElement = fixture.debugElement.query(By.css('label')).nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be invalid when empty', () => {
        expect(acceptTermsControl.valid).toBeFalsy();
      });

      it('should be required', () => {
        errors = acceptTermsControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });
    });

    describe('#signUpButton', () => {
      it('should render button for submit form', () => {
        nativeElement = fixture.debugElement.query(By.css('button[type=submit]')).nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should call onSignUp() method', () => {
        nativeElement = fixture.debugElement.query(By.css('form'));

        jest.spyOn(signUpComponent, 'onSignUp');
        nativeElement.triggerEventHandler('submit', null);
        expect(signUpComponent.onSignUp).toHaveBeenCalled();
      });

      it('should set submitted to true', () => {
        expect(signUpComponent.submitted).toBe(false);

        signUpComponent.onSignUp();
        expect(signUpComponent.submitted).toBe(true);
      });

      it('should not sign up if form is invalid', () => {
        emailControl.setValue('test');
        passwordControl.setValue('1234');
        passwordConfirmationControl.setValue('15948');

        signUpComponent.onSignUp();
        expect(signUpComponent.onSignUp()).toBe(undefined);
      });
    });

    describe('#singInLink', () => {
      beforeEach(() => {
        nativeElement = fixture.debugElement.query(By.css('a[id=sign-in]')).nativeElement;
      });

      it('should render a link to sign up', () => {
        expect(nativeElement).toBeTruthy();
      });

      it('should navigate to sign up', fakeAsync(() => {
        const href = nativeElement.getAttribute('href');
        expect(href).toEqual('/auth/sign-in');
      }));
    });
  });
});
