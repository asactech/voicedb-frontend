import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { AuthService } from '../../core';
import { AlertService, ConfirmPasswordValidator, FormErrorHandler, errorMessages } from '../../shared';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  public signUpForm: FormGroup;
  public faTimes = faTimes;
  public submitted = false;
  public error = errorMessages.user;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private alertService: AlertService
  ) {}

  public ngOnInit() {
    this.signUpForm = this.formBuilder.group(
      {
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', [Validators.required]],
        accepTerms: [false, [Validators.requiredTrue]]
      },
      {
        validator: ConfirmPasswordValidator.MatchPassword
      }
    );
  }

  public get email() {
    return this.signUpForm.get('email');
  }

  public get password() {
    return this.signUpForm.get('password');
  }

  public get confirmPassword() {
    return this.signUpForm.get('confirmPassword');
  }

  public get accepTerms() {
    return this.signUpForm.get('accepTerms');
  }

  public onSignUp() {
    this.submitted = true;

    if (!FormErrorHandler.validateForm(this.signUpForm)) {
      return;
    }

    this.authService.signUp(this.signUpForm.value).subscribe(
      message => {
        this.alertService.success(message);
        setTimeout(() => {
          this.router.navigate(['/auth/sign-in']);
        }, 4000);
      },
      error => {
        if (error instanceof Object) {
          FormErrorHandler.errorHandler(this.signUpForm, error);
        } else {
          this.alertService.error(error);
        }
      }
    );
  }
}
