import { AbstractControl } from '@angular/forms';

export class ConfirmPasswordValidator {
  static MatchPassword(control: AbstractControl) {
    let password = control.get('password')!.value;
    const confirmPassword = control.get('confirmPassword')!.value;

    if (control.get('newPassword')) {
      password = control.get('newPassword')!.value;
    }

    if (password !== confirmPassword) {
      control.get('confirmPassword')!.setErrors({ confirmpassword: true });
    } else {
      return;
    }
  }
}
