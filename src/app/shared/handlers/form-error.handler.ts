import { FormGroup } from '@angular/forms';

export class FormErrorHandler {
  static errorHandler(formGroup: FormGroup, error: Object): any {
    Object.keys(error).forEach(prop => {
      const formControl = formGroup.get(prop);

      if (formControl) {
        formControl.setErrors({
          servererror: error[prop]
        });
      }
    });
  }

  static validateForm(formGroup: FormGroup) {
    if (formGroup.invalid) {
      Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        control.markAsTouched({ onlySelf: true });
      });

      return false;
    }

    return true;
  }
}

function genericMessages(
  option: string[],
  minlength?: number,
): Object {
  const messages = {
    required: `Este campo es requerido.`,
    minlength: `Debe tener de ${minlength} dígitos.`,
  };

  switch (option[0]) {
    case 'all':
      return messages;
    default:
      return Object.keys(messages)
        .filter(key => option.includes(key))
        .reduce((obj, key) => {
          return { ...obj, [key]: messages[key] };
        }, {});
  }
}

export const errorMessages = {
  user: {
    email: {
      ...genericMessages(['required']),
      email: 'El formato debe ser alias@dominio.com'
    },
    password: {
      ...genericMessages(['all'], 6),
      confirmpassword: 'Las contraseñas no coinciden'
    },
    accepTerms: genericMessages(['required']),
    fullName: genericMessages(['required'])
  },
  model: {
    name: genericMessages(['required'])
  }
};
