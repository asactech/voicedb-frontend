import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoaderService } from '../../services';
import { LoaderComponent } from './loader.component';

describe('LoaderComponent', () => {
  let loaderComponent: LoaderComponent;
  let fixture: ComponentFixture<LoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoaderComponent],
      providers: [LoaderService]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(LoaderComponent);
        loaderComponent = fixture.componentInstance;
        loaderComponent.ngOnInit();
      });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(loaderComponent).toBeTruthy();
  });
});
