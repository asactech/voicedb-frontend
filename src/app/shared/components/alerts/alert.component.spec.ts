import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AlertService } from '../../services';
import { AlertComponent } from './alert.component';

describe('AlertComponent', () => {
  let alertComponent: AlertComponent;
  let fixture: ComponentFixture<AlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AlertComponent],
      imports: [RouterTestingModule, NgbModule],
      providers: [AlertService]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(AlertComponent);
        alertComponent = fixture.componentInstance;
        alertComponent.ngOnInit();
      });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(alertComponent).toBeTruthy();
  });
});
