import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { Alert } from '../../models';
import { AlertService } from '../../services';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit, OnDestroy {
  @Input() public isAuthenticated: boolean;

  private subscription: Subscription;

  public alert: Alert;
  public staticAlertClosed = false;

  constructor(private alertService: AlertService) {}

  public ngOnInit() {
    this.subscription = this.alertService.getAlert().subscribe(alert => {
      this.alert = alert;
      if (this.alert) {
        setTimeout(() => this.onCloseAlert(this.alert), 8000);
      }
    });
  }

  public onCloseAlert(alert: Alert) {
    this.alert = null;
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
