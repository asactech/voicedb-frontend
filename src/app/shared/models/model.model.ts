export interface Link {
  from: string;
  to: string;
  fromArrow: string;
  toArrow: string;
}

export interface Items {
  name: string;
  isKey?: boolean;
}

export interface Node {
  key: string;
  items?: Array<Items>;
}

export interface Model {
  _id?: string;
  userId?: string;
  name?: string;
  preview?: string;
  updatedModelAt?: string;
  updatedAt?: string;
  createdAt?: string;
  nodeDataArray?: Array<Node>;
  linkDataArray?: Array<Link>;
}
