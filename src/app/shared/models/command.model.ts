export interface Command {
    action: string;
    target: string;
    entities?: any[];
    relations?: any[];
    mandatory?: string[];
    cardinality?: string[];
    args?: any[];
}