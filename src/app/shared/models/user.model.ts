export interface User {
  id?: string;
  email?: string;
  password?: string;
  confirmPassword?: string;
  fullName?: string;
  birthDate?: any;
  occupation?: string;
  companyOrEducation?: string;
  country?: string;
  avatar?: string;
  token?: string;
  expiresAt?: string;
}
