import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { LoaderState } from '../../models';

@Injectable()
export class LoaderService {

  private loaderStateSubject = new Subject<LoaderState>();
  loaderState = this.loaderStateSubject.asObservable();

  constructor() { }

  public show() {
    this.loaderStateSubject.next(<LoaderState>{ show: true});
  }

  public hide() {
    this.loaderStateSubject.next(<LoaderState>{ show: false});
  }
}
