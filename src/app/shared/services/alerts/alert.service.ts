import { Injectable } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { Alert } from '../../models';

@Injectable()
export class AlertService {
  private alertSubject = new Subject<Alert>();
  private keepAfterNavigationChange = false;

  constructor(private router: Router) {
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this.keepAfterNavigationChange) {
          this.keepAfterNavigationChange = false;
        } else {
          this.alertSubject.next();
        }
      }
    });
  }

  public success(message: string, keepAfterNavigationChange = false) {
    this.keepAfterNavigationChange = keepAfterNavigationChange;
    this.alertSubject.next(<Alert>{ type: 'success', message: message });
  }

  public error(message: string, keepAfterNavigationChange = false) {
    this.keepAfterNavigationChange = keepAfterNavigationChange;
    this.alertSubject.next(<Alert>{ type: 'error', message: message });
  }

  public getAlert(): Observable<Alert> {
    return this.alertSubject.asObservable();
  }
}
