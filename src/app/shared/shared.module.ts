import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { AlertComponent, LoaderComponent } from './components';
import { AlertService, LoaderService } from './services';

@NgModule({
  declarations: [
    AlertComponent,
    LoaderComponent,
  ],
  imports: [
    CommonModule,
    NgbAlertModule,
  ],
  providers: [
    AlertService,
    LoaderService,
  ],
  exports: [
    AlertComponent,
    LoaderComponent,
  ]
})
export class SharedModule { }
