import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyModelsComponent } from './my-models.component';

import { AuthGuard } from '../core';

export const routes: Routes = [
  { path: 'mis-modelos', component: MyModelsComponent, canActivate: [ AuthGuard ] },
  { path: '**', redirectTo: 'auth' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyModelsRoutingModule { }
