import { AfterViewInit, Component, Input, OnInit } from '@angular/core';

import { Model } from '../../shared';

@Component({
  selector: 'app-saved',
  templateUrl: './saved.component.html',
  styleUrls: ['./saved.component.scss']
})
export class SavedComponent {
  @Input() public models: Array<Model>;
  @Input() public isEmpty = false;

  constructor() { }

}
