import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faBezierCurve, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModelService } from 'src/app/core';
import { AlertService, Model } from '../../../shared';
import { AddModelModalComponent } from '../../add-model-modal/add-model-modal.component';


@Component({
  selector: 'app-model-card',
  templateUrl: './model-card.component.html',
  styleUrls: ['./model-card.component.scss']
})
export class ModelCardComponent implements OnInit {
  @Input() public model: Model;
  readonly modelPreview: string = '../../assets/img/default_model.png';

  public faBezierCurve = faBezierCurve;
  public faEdit = faEdit;
  public faTrash = faTrash;
  public updateModelModalRef: NgbModalRef;

  constructor(
    private modelService: ModelService,
    private alertService: AlertService,
    private router: Router,
    private modalService: NgbModal
  ) {}

  public ngOnInit() {}

  public onLoad() {
    const fields = '_id,name,nodeDataArray,linkDataArray';

    this.modelService.load(this.model._id, fields).subscribe(
      () => {
        this.router.navigate(['/']);
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public onUpdate() {
    this.updateModelModalRef = this.modalService.open(AddModelModalComponent);
    this.updateModelModalRef.componentInstance.isUpdate = true;
    this.updateModelModalRef.componentInstance.modelId = this.model._id;
    this.updateModelModalRef.componentInstance.modelName = this.model.name;
  }

  public onDelete() {
    this.modelService.delete(this.model._id).subscribe(
      message => {
        this.alertService.success(message);

        this.router
          .navigateByUrl('/MyModelsComponent', { skipLocationChange: true })
          .then(() => this.router.navigate(['/mis-modelos']));
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
}
