import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { Model } from 'src/app/shared';

import { ModelCardComponent } from './model-card.component';
import { By } from '@angular/platform-browser';

describe('ModelCardComponent', () => {
  let modelCardComponent: ModelCardComponent;
  let fixture: ComponentFixture<ModelCardComponent>;
  let model: Model;
  let nativeElement: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModelCardComponent]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ModelCardComponent);
        modelCardComponent = fixture.componentInstance;

        model = {
          id:            'ASD456SD4F5SD4AD',
          userId:         'ASD456SD4F5SD4AD',
          preview:        '../../assets/img/default_model.jpg',
          name:           'Test Model',
          updatedModelAt: '03/03/2003',
          updatedAt:      '02/02/2002',
          createdAt:      '01/01/2001',
        };
        modelCardComponent.model = model;
      });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(modelCardComponent).toBeTruthy();
  });

  describe('#openLink', () => {
    beforeEach(() => {
      nativeElement = fixture.debugElement.query(By.css('a[id=open]')).nativeElement;
    });

    it('should render a link to open model', () => {
      expect(nativeElement).toBeTruthy();
    });

    it('should navigate to home', fakeAsync(() => {
      const href = nativeElement.getAttribute('href');
      expect(href).toEqual(null);
    }));
  });

  describe('#cardContent', () => {
    it('should render image for model preview', () => {
      nativeElement = fixture.debugElement.query(By.css('a[id=open]')).nativeElement;
      expect(nativeElement).toBeTruthy();
    });

    it('should render h5 tag for model name', () => {
      nativeElement = fixture.debugElement.query(By.css('h5')).nativeElement;
      expect(nativeElement).toBeTruthy();
    });

    it('should render p tag for created date', () => {
      nativeElement = fixture.debugElement.query(By.css('p[id=created]')).nativeElement;
      expect(nativeElement).toBeTruthy();
    });

    it('should render p tag for updated model at', () => {
      nativeElement = fixture.debugElement.query(By.css('p[id=updated]')).nativeElement;
      expect(nativeElement).toBeTruthy();
    });
  });
});
