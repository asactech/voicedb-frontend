import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModalModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared';
import { AddModelModalComponent } from './add-model-modal/add-model-modal.component';
import { MyModelsRoutingModule } from './my-models-routing.module';
import { MyModelsComponent } from './my-models.component';
import { ModelCardComponent } from './saved/model-card/model-card.component';
import { SavedComponent } from './saved/saved.component';
import { AuthGuard } from '../core';

@NgModule({
  declarations: [
    MyModelsComponent,
    SavedComponent,
    ModelCardComponent,
    AddModelModalComponent
  ],
  imports: [
    CommonModule,
    MyModelsRoutingModule,
    NgbTabsetModule,
    SharedModule,
    FontAwesomeModule,
    NgbModalModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [AddModelModalComponent],
  providers: [
    AuthGuard
  ]
})
export class MyModelsModule {}
