import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModelService } from 'src/app/core';
import { AlertService, errorMessages, Model, FormErrorHandler } from 'src/app/shared';

@Component({
  selector: 'app-add-model-modal',
  templateUrl: './add-model-modal.component.html',
  styleUrls: ['./add-model-modal.component.scss']
})
export class AddModelModalComponent implements OnInit {
  @Input() public isUpdate: boolean;
  @Input() public modelId: string;
  @Input() public modelName: string;

  public faTimes = faTimes;
  public addModelForm: FormGroup;
  public error = errorMessages.model;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private modelService: ModelService,
    private alertService: AlertService,
    private router: Router
  ) {}

  public ngOnInit() {
    this.addModelForm = this.formBuilder.group({
      name: ['', [Validators.required]]
    });

    this.addModelForm.get('name').patchValue(this.modelName);
  }

  public get name() {
    return this.addModelForm.get('name');
  }

  public onAdd() {
    if (!FormErrorHandler.validateForm(this.addModelForm)) {
      return;
    }

    this.modelService.add(this.addModelForm.value).subscribe(
      () => {
        this.activeModal.dismiss();

        this.router.navigate(['/']);
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public onUpdate() {
    if (!FormErrorHandler.validateForm(this.addModelForm)) {
      return;
    }

    this.modelService.update(this.modelId, this.name.value, undefined, undefined).subscribe(
      message => {
        this.alertService.success(message);

        this.activeModal.dismiss();

        this.router
          .navigateByUrl('/MyModelsComponent', { skipLocationChange: true })
          .then(() => this.router.navigate(['/mis-modelos']));
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
}
