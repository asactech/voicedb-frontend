import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { ModelService } from '../core';
import { AlertService, Model } from '../shared';
import { AddModelModalComponent } from './add-model-modal/add-model-modal.component';

@Component({
  selector: 'app-my-models',
  templateUrl: './my-models.component.html',
  styleUrls: ['./my-models.component.scss']
})
export class MyModelsComponent implements OnInit {
  public models: Array<Model> = [];
  public faPlus = faPlus;
  public addModelModalRef: NgbModalRef;

  private fields = 'id,name,preview,createdAt,updatedModelAt';

  constructor(
    private modelService: ModelService,
    private alertService: AlertService,
    private modalService: NgbModal
  ) {}

  public ngOnInit() {
    this.modelService.getModels(this.fields).subscribe(
      models => {
        this.models = models;
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public openAddModelModal() {
    this.addModelModalRef = this.modalService.open(AddModelModalComponent);
  }
}
