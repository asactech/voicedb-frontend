import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared';
import { routes } from './my-models-routing.module';
import { MyModelsComponent } from './my-models.component';
import { ModelCardComponent } from './saved/model-card/model-card.component';
import { SavedComponent } from './saved/saved.component';

describe('MyModelsComponent', () => {
  let myModelsComponent: MyModelsComponent;
  let fixture: ComponentFixture<MyModelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MyModelsComponent, SavedComponent, ModelCardComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(routes),
        NgbModule,
        SharedModule
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(MyModelsComponent);
        myModelsComponent = fixture.componentInstance;
      });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(myModelsComponent).toBeTruthy();
  });
});
