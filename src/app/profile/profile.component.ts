import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

import { UserService } from '../core';
import { AlertService, User } from '../shared';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [NgbTabsetConfig]
})
export class ProfileComponent implements OnInit, OnDestroy {
  public user: User;

  private userSubjectListener: Subscription;
  readonly avatarURL: any = '../../assets/img/default_avatar.png';

  constructor(
    private ngbTabsetConfig: NgbTabsetConfig,
    private userService: UserService,
    private alertService: AlertService
  ) {
    ngbTabsetConfig.justify = 'start';
    ngbTabsetConfig.type = 'tabs';
  }

  public ngOnInit() {
    this.userSubjectListener = this.userService
      .getUserSubject()
      .subscribe(user => (this.user = user));
  }

  public avatarPreview(files) {
    const file: File = files[0];
    const mimeType = file.type;
    const reader = new FileReader();

    if (files.length === 0 || mimeType.match(/image\/*/) === null) {
      return;
    }

    reader.addEventListener('load', (event: any) => {
      this.userService.uploadAvatar(file).subscribe(
        avatar => {
          this.user.avatar = avatar;
        },
        error => {
          this.alertService.error(error);
        }
      );
    });

    reader.readAsDataURL(file);
  }

  public ngOnDestroy() {
    this.userSubjectListener.unsubscribe();
  }
}
