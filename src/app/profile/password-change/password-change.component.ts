import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserService } from '../../core';
import { AlertService, ConfirmPasswordValidator, FormErrorHandler, errorMessages } from '../../shared';

@Component({
  selector: 'app-password-change',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.component.scss']
})
export class PasswordChangeComponent implements OnInit {
  public changePasswordForm: FormGroup;
  public error = errorMessages.user;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private alertService: AlertService
  ) {}

  public ngOnInit() {
    this.changePasswordForm = this.formBuilder.group(
      {
        password: ['', [Validators.required, Validators.minLength(6)]],
        newPassword: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', [Validators.required]]
      },
      {
        validator: ConfirmPasswordValidator.MatchPassword
      }
    );
  }

  public get password() {
    return this.changePasswordForm.get('password');
  }

  public get newPassword() {
    return this.changePasswordForm.get('newPassword');
  }

  public get confirmPassword() {
    return this.changePasswordForm.get('confirmPassword');
  }

  public onUpdate() {
    if (!FormErrorHandler.validateForm(this.changePasswordForm)) {
      return;
    }

    this.userService.changePassword(this.password.value, this.confirmPassword.value).subscribe(
      message => {
        this.alertService.success(message);
        this.changePasswordForm.reset();
      },
      error => {
        if (error instanceof Object) {
          FormErrorHandler.errorHandler(this.changePasswordForm, error);
        } else {
          this.alertService.error(error);
        }
      }
    );
  }
}
