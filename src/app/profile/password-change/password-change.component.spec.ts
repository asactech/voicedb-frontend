import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AbstractControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/app/shared';

import { PasswordChangeComponent } from './password-change.component';

describe('PasswordChangeComponent', () => {
  let passwordChangeComponent: PasswordChangeComponent;
  let fixture: ComponentFixture<PasswordChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PasswordChangeComponent],
      imports: [
        HttpClientModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(PasswordChangeComponent);
        passwordChangeComponent = fixture.componentInstance;
        passwordChangeComponent.ngOnInit();
      });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(passwordChangeComponent).toBeTruthy();
  });

  describe('#changePasswordForm', () => {
    let nativeElement: any;
    let passwordControl: AbstractControl;
    let newPasswordControl: AbstractControl;
    let passwordConfirmationControl: AbstractControl;
    let errors = {};

    beforeEach(() => {
      passwordControl = passwordChangeComponent.changePasswordForm.controls['password'];
      newPasswordControl = passwordChangeComponent.changePasswordForm.controls['newPassword'];
      passwordConfirmationControl =
        passwordChangeComponent.changePasswordForm.controls['confirmPassword'];
    });

    it('should be invalid when empty', () => {
      expect(passwordChangeComponent.changePasswordForm.valid).toBeFalsy();
    });

    describe('#passwordInput', () => {
      it('should render input field for actual password', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=password]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be invalid when empty', () => {
        expect(passwordControl.valid).toBeFalsy();
      });

      it('should be required', () => {
        errors = passwordControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });

      it('should be a valid password', () => {
        passwordControl.setValue('1234');
        errors = passwordControl.errors || {};
        expect(errors['minlength']).toBeTruthy();
      });
    });

    describe('#newPasswordInput', () => {
      it('should render input field for new password', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=newPassword]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be invalid when empty', () => {
        expect(newPasswordControl.valid).toBeFalsy();
      });

      it('should be required', () => {
        errors = newPasswordControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });

      it('should be a valid password', () => {
        newPasswordControl.setValue('1234');
        errors = newPasswordControl.errors || {};
        expect(errors['minlength']).toBeTruthy();
      });
    });

    describe('#passwordConfirmationInput', () => {
      it('should render input field for confirm password', () => {
        nativeElement = fixture.debugElement.query(
          By.css('input[formControlName=confirmPassword]')
        ).nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be invalid when empty', () => {
        expect(passwordConfirmationControl.valid).toBeFalsy();
      });

      it('should be required', () => {
        errors = passwordConfirmationControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });

      it('should be equal than password', () => {
        newPasswordControl.setValue('123456');
        passwordConfirmationControl.setValue('159487');
        errors = passwordConfirmationControl.errors || {};
        expect(errors['confirmpassword']).toBeTruthy();
      });
    });

    describe('#updateButton', () => {
      it('should render button for submit form', () => {
        nativeElement = fixture.debugElement.query(By.css('button[type=submit]')).nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should call onSignUp() method', () => {
        nativeElement = fixture.debugElement.query(By.css('form'));

        jest.spyOn(passwordChangeComponent, 'onUpdate');
        nativeElement.triggerEventHandler('submit', null);
        expect(passwordChangeComponent.onUpdate).toHaveBeenCalled();
      });

      it('should not change password if form is invalid', () => {
        passwordControl.setValue('123456');
        newPasswordControl.setValue('789456');
        passwordConfirmationControl.setValue('123456');

        passwordChangeComponent.onUpdate();
        expect(passwordChangeComponent.onUpdate()).toBe(undefined);
      });
    });
  });
});
