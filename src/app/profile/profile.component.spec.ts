import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared';
import { InformationComponent } from './information/information.component';
import { PasswordChangeComponent } from './password-change/password-change.component';
import { routes } from './profile-routing.module';
import { ProfileComponent } from './profile.component';

describe('ProfileComponent', () => {
  let profileComponent: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileComponent, InformationComponent, PasswordChangeComponent],
      imports: [
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes(routes),
        FontAwesomeModule,
        NgbModule,
        SharedModule
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ProfileComponent);
        profileComponent = fixture.componentInstance;
      });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(profileComponent).toBeTruthy();
  });
});
