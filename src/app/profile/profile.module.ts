import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared';
import { ProfileComponent } from './profile.component';
import { InformationComponent } from './information/information.component';
import { PasswordChangeComponent } from './password-change/password-change.component';
import { AuthGuard } from '../core';

@NgModule({
  declarations: [
    ProfileComponent,
    InformationComponent,
    PasswordChangeComponent,
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule,
    NgbTabsetModule,
    NgbDatepickerModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    AuthGuard
  ]
})
export class ProfileModule { }
