import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AbstractControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule, User } from 'src/app/shared';

import { InformationComponent } from './information.component';

describe('InformationComponent', () => {
  let informationComponent: InformationComponent;
  let fixture: ComponentFixture<InformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InformationComponent],
      imports: [
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        FontAwesomeModule,
        NgbModule,
        SharedModule
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(InformationComponent);
        informationComponent = fixture.componentInstance;
      });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(informationComponent).toBeTruthy();
  });

  describe('#informationForm', () => {
    let nativeElement: any;
    let fullNameControl: AbstractControl;
    let emailControl: AbstractControl;
    let errors = {};

    it('should be invalid when empty', () => {
      expect(informationComponent.informationForm.valid).toBeFalsy();
    });

    beforeEach(() => {
      fullNameControl = informationComponent.informationForm.controls['fullName'];
      emailControl = informationComponent.informationForm.controls['email'];
    });

    describe('#fullNameInput', () => {
      it('should render input field for full name', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=fullName]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be invalid when empty', () => {
        expect(fullNameControl.valid).toBeFalsy();
      });

      it('should be required', () => {
        errors = fullNameControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });
    });

    describe('#emailInput', () => {
      it('should render input field for email', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=email]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should be invalid when empty', () => {
        expect(emailControl.valid).toBeFalsy();
      });

      it('should be required', () => {
        errors = emailControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });

      it('should be email', () => {
        emailControl.setValue('test');
        errors = emailControl.errors || {};
        expect(errors['email']).toBeTruthy();
      });
    });

    describe('#birthDateInput', () => {
      it('should render input field for birthdate', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=birthDate]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });
    });

    describe('#occupationInput', () => {
      it('should render input field for occupation', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=occupation]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });
    });

    describe('#companyOrEducationInput', () => {
      it('should render input field for company or education', () => {
        nativeElement = fixture.debugElement.query(
          By.css('input[formControlName=companyOrEducation]')
        ).nativeElement;
        expect(nativeElement).toBeTruthy();
      });
    });

    describe('#countryInput', () => {
      it('should render input field for country', () => {
        nativeElement = fixture.debugElement.query(By.css('input[formControlName=country]'))
          .nativeElement;
        expect(nativeElement).toBeTruthy();
      });
    });

    describe('#updateButton', () => {
      it('should render button for submit form', () => {
        nativeElement = fixture.debugElement.query(By.css('button[type=submit]')).nativeElement;
        expect(nativeElement).toBeTruthy();
      });

      it('should not send data if form is invalid', () => {
        fullNameControl.setValue('test user');
        emailControl.setValue('test@user.com');

        informationComponent.onUpdate();
        expect(informationComponent.onUpdate()).toBe(undefined);
      });

      it('should call onUpdate() method', () => {
        nativeElement = fixture.debugElement.query(By.css('form'));

        jest.spyOn(informationComponent, 'onUpdate');
        nativeElement.triggerEventHandler('submit', null);
        expect(informationComponent.onUpdate).toHaveBeenCalled();
      });
    });
  });
});
