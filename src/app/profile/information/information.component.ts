import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { faCalendar } from '@fortawesome/free-solid-svg-icons';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

import { UserService } from '../../core';
import { AlertService, FormErrorHandler, errorMessages } from '../../shared';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss'],
  providers: [NgbDatepickerConfig]
})
export class InformationComponent implements OnInit {
  public faCalendar = faCalendar;
  public informationForm: FormGroup;
  public error = errorMessages.user;

  private fields = 'fullName,email,birthDate,occupation,companyOrEducation,country,avatar';

  constructor(
    private ngbDatepickerConfig: NgbDatepickerConfig,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private alertService: AlertService
  ) {
    this.ngbDatepickerConfig.minDate = { year: 1919, month: 1, day: 1 };
    this.ngbDatepickerConfig.maxDate = { year: 2119, month: 1, day: 1 };
    this.ngbDatepickerConfig.outsideDays = 'hidden';
  }

  public ngOnInit() {
    this.informationForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birthDate: [''],
      occupation: [''],
      companyOrEducation: [''],
      country: ['']
    });

    this.userService.getUser(this.fields).subscribe(
      user => {
        this.informationForm.patchValue(user);
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public get fullName() {
    return this.informationForm.get('fullName');
  }

  public get email() {
    return this.informationForm.get('email');
  }

  public get birthDate() {
    return this.informationForm.get('birthDate');
  }

  public get occupation() {
    return this.informationForm.get('occupation');
  }

  public get companyOrEducation() {
    return this.informationForm.get('companyOrEducation');
  }

  public get country() {
    return this.informationForm.get('country');
  }

  public onUpdate() {
    if (!FormErrorHandler.validateForm(this.informationForm)) {
      return;
    }

    this.userService.updateUser(this.informationForm.value).subscribe(
      message => {
        this.alertService.success(message);
      },
      error => {
        if (error instanceof Object) {
          FormErrorHandler.errorHandler(this.informationForm, error);
        } else {
          this.alertService.error(error);
        }
      }
    );
  }
}
