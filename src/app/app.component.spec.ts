import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { routes } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth';
import { AuthInterceptor, CoreModule, ErrorInterceptor, LoaderInterceptor } from './core';
import { HomeModule } from './home';
import { SharedModule } from './shared';

describe('AppComponent', () => {
  let appComponent: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [
        RouterTestingModule.withRoutes(routes),
        HttpClientModule,
        CoreModule,
        SharedModule,
        AuthModule,
        HomeModule
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: LoaderInterceptor,
          multi: true
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ErrorInterceptor,
          multi: true
        }
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(AppComponent);
        appComponent = fixture.debugElement.componentInstance;
        appComponent.ngOnInit();
      });
  }));

  it('should be created', () => {
    expect(appComponent).toBeTruthy();
  });
});
