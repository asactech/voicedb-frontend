import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from './core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  public isAuthenticated: boolean;
  private authenticationStatusListener: Subscription;

  constructor(private authService: AuthService) {
    this.authService.isLoggedIn();
    this.isAuthenticated = this.authService.isAuthenticated;
  }

  public ngOnInit() {
    this.authenticationStatusListener = this.authService
      .getAuthStatusSubject()
      .subscribe(isAuthenticated => {
        this.isAuthenticated = isAuthenticated;
      });
  }

  public ngOnDestroy() {
    this.authenticationStatusListener.unsubscribe();
  }
}
