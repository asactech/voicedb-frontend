import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { SharedModule } from '../shared';
import { routes } from './home-routing.module';
import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let homeComponent: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let microphoneButton: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent],
      imports: [RouterTestingModule.withRoutes(routes), FontAwesomeModule, SharedModule]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(HomeComponent);
        homeComponent = fixture.componentInstance;
      });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(homeComponent).toBeTruthy();
  });

  describe('#microphoneButton', () => {
    it('should render button for submit form', () => {
      microphoneButton = fixture.debugElement.query(By.css('button')).nativeElement;
      expect(microphoneButton).toBeTruthy();
    });

    it('should activate the microphone and start recording', () => {
      expect(homeComponent.microphoneIsActive).toBe(false);

      homeComponent.onActive();
      expect(homeComponent.microphoneIsActive).toBe(true);
    });

    it('should deactivate the microphone', () => {
      homeComponent.onActive();
      expect(homeComponent.microphoneIsActive).toBe(true);

      homeComponent.onActive();
      expect(homeComponent.microphoneIsActive).toBe(false);
    });
  });
});
