import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import * as go from 'gojs';

@Component({
  selector: 'app-model-editor',
  templateUrl: './model-editor.component.html',
  styleUrls: ['./model-editor.component.scss']
})
export class ModelEditorComponent implements OnInit {
  private diagram: go.Diagram = new go.Diagram();

  @ViewChild('diagramDiv', { static: true })
  private diagramRef: ElementRef;

  @Input()
  get model(): go.Model {
    return this.diagram.model;
  }
  set model(val: go.Model) {
    this.diagram.model = val;
  }

  @Output()
  nodeSelected = new EventEmitter<go.Node | null>();

  @Output()
  modelChanged = new EventEmitter<go.ChangedEvent>();

  constructor() {
    const $ = go.GraphObject.make;

    // define several shared Brushes
    const lightgrad = $(go.Brush, 'Linear', { 1: '#E6E6FA', 0: '#FFFAF0' });

    // the template for each attribute in a node's array of item data
    const itemTempl = $(
      go.Panel,
      'Horizontal',
      // $(go.Shape,
      //   { desiredSize: new go.Size(10, 10) },
      //   new go.Binding('figure', 'figure'),
      //   new go.Binding('fill', 'color')),
      $(
        go.TextBlock,
        {
          stroke: '#4C595C',
          font: 'bold 14px sans-serif'
        },
        new go.Binding('text', 'name')
      )
    );

    this.diagram = new go.Diagram();
    this.diagram.initialContentAlignment = go.Spot.Center;
    this.diagram.allowDelete = false;
    this.diagram.allowCopy = false;
    this.diagram.undoManager.isEnabled = true;
    this.diagram.layout = $(go.ForceDirectedLayout);
    this.diagram.addDiagramListener('ChangedSelection', e => {
      const node = e.diagram.selection.first();
      this.nodeSelected.emit(node instanceof go.Node ? node : null);
    });
    this.diagram.addModelChangedListener(
      e => e.isTransactionFinished && this.modelChanged.emit(e)
    );

    // define the Node template, representing an entity
    this.diagram.nodeTemplate = $(
      go.Node,
      'Auto', // the whole node panel
      {
        selectionAdorned: true,
        resizable: true,
        layoutConditions: go.Part.LayoutStandard & ~go.Part.LayoutNodeSized,
        fromSpot: go.Spot.AllSides,
        toSpot: go.Spot.AllSides,
        isShadowed: true,
        shadowColor: '#E0E0E0'
      },
      new go.Binding('location', 'loc', go.Point.parse).makeTwoWay(
        go.Point.stringify
      ),
      // whenever the PanelExpanderButton changes the visible property of the 'LIST' panel,
      // clear out any desiredSize set by the ResizingTool.
      new go.Binding('desiredSize', 'visible', function(v) {
        return new go.Size(NaN, NaN);
      }).ofObject('LIST'),
      // define the node's outer shape, which will surround the Table
      $(
        go.Shape,
        'RoundedRectangle',
        {
          fill: '#ffffff',
          stroke: '#4C595C',
          strokeWidth: 1.5
        }
      ),
      $(
        go.Panel,
        'Table',
        {
          margin: 8,
          stretch: go.GraphObject.Fill
        },
        $(go.RowColumnDefinition, {
          row: 0,
          sizing: go.RowColumnDefinition.None
        }),
        // the table header
        $(
          go.TextBlock,
          {
            row: 0,
            stroke: '#00070C',
            alignment: go.Spot.Center,
            margin: new go.Margin(0, 14, 0, 2), // leave room for Button
            font: 'bold 16px sans-serif'
          },
          new go.Binding('text', 'key')
        ),
        // the collapse/expand button
        $(
          'PanelExpanderButton',
          'LIST', // the name of the element whose visibility this button toggles
          { row: 0, alignment: go.Spot.TopRight }
        ),
        // the list of Panels, each showing an attribute
        $(
          go.Panel,
          'Vertical',
          {
            name: 'LIST',
            row: 1,
            padding: 3,
            alignment: go.Spot.TopLeft,
            defaultAlignment: go.Spot.Left,
            stretch: go.GraphObject.Horizontal,
            itemTemplate: itemTempl
          },
          new go.Binding('itemArray', 'items')
        )
      ) // end Table Panel
    ); // end Node
    // define the Link template, representing a relationship
    this.diagram.linkTemplate = $(
      go.Link, // the whole link panel
      {
        selectionAdorned: true,
        layerName: 'Foreground',
        reshapable: true,
        routing: go.Link.AvoidsNodes,
        corner: 5,
        curve: go.Link.JumpOver
      },
      $(
        // the link shape
        go.Shape,
        {
          stroke: '#00070C',
          strokeWidth: 1.5
        }
      ),
      $(
        // the "from" arrowhead
        go.Shape,
        new go.Binding('fromArrow', 'fromArrow'),
        {
          scale: 2,
          stroke: '#00070C',
          fill: '#FFFFFF'
        }
      ),
      $(
        go.Shape, // the "from" arrowhead
        new go.Binding('toArrow', 'toArrow'),
        {
          scale: 2,
          stroke: '#00070C',
          fill: '#FFFFFF'
        }
      )
      // $(
      //   go.TextBlock, // the 'from' label
      //   {
      //     textAlign: 'center',
      //     font: 'bold 14px sans-serif',
      //     stroke: '#007C57',
      //     segmentIndex: 0,
      //     segmentOffset: new go.Point(NaN, NaN),
      //     segmentOrientation: go.Link.OrientUpright
      //   },
      //   new go.Binding('text', 'text')
      // ),
      // $(
      //   go.TextBlock, // the 'to' label
      //   {
      //     textAlign: 'center',
      //     font: 'bold 14px sans-serif',
      //     stroke: '#007C57',
      //     segmentIndex: -1,
      //     segmentOffset: new go.Point(NaN, NaN),
      //     segmentOrientation: go.Link.OrientUpright
      //   },
      //   new go.Binding('text', 'toText')
      // )
    );
  }

  ngOnInit() {
    this.diagram.div = this.diagramRef.nativeElement;
  }
}
