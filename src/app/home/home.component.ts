import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  faMicrophone,
  faMicrophoneSlash,
  faSave
} from '@fortawesome/free-solid-svg-icons';
import * as go from 'gojs';
import { Subscription, timer } from 'rxjs';

import { ModelService, SpeechRecognitionService } from '../core';
import { NlpService } from '../core/services/home/nlp.service';
import {
  Action,
  ModelManagementService,
  Target
} from '../core/services/model/model-management.service';
import { AlertService, Model } from '../shared';
import { Command } from '../shared/models/command.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  private modelSubjectListener: Subscription;
  private currentModelSubjectListener: Subscription;
  private currentModel: Model;

  private data: any;
  private node: go.Node;
  private created = false;

  public microphoneIsActive: boolean;
  public faMicrophone = faMicrophoneSlash;
  public faSave = faSave;
  public speechData: string;
  public model = new go.GraphLinksModel();
  public timerSubscription: Subscription;

  constructor(
    private speechRecognitionService: SpeechRecognitionService,
    private modelService: ModelService,
    private modelManagement: ModelManagementService,
    private nlpService: NlpService,
    private alertService: AlertService
  ) {
    this.microphoneIsActive = false;
    this.speechData = '';
    this.model = modelManagement.model;
    this.currentModel = this.modelService.currentModel;
  }

  public ngOnInit() {

    this.currentModelSubjectListener = this.modelService
    .getCurrentModelSubject()
      .subscribe(currentModel => (this.currentModel = currentModel));

      this.modelSubjectListener = this.modelManagement
      .getModelSubject()
      .subscribe(model => (this.model = model));

    this.model.addChangedListener(evt => {
      if (evt.isTransactionFinished) {
        if (this.model.nodeDataArray.length > 0) {
          this.timerSubscription ? this.timerSubscription.unsubscribe() : null;
          this.startTimer();
        }
      }
    });
  }

  private startTimer() {
    const timerSource = timer(0, 500);

    this.timerSubscription = timerSource.subscribe(number => {
      if (30 === number) {
        if (this.currentModel && !this.currentModel._id && !this.created) {
          this.created = true;

          const date = new Date();

          let model: Model;
          model = {
            name:
              'model' +
              '_' +
              date.getFullYear().toString() +
              (date.getMonth() + 1).toString() +
              date.getDate().toString() +
              '_' +
              date.getHours().toString() +
              date.getMinutes().toString() +
              date.getSeconds().toString()
          };

          this.onAdd(model);
        } else {
          this.onUpdate();
        }
      }
    });
  }

  public onActive() {
    if (!this.microphoneIsActive) {
      this.microphoneIsActive = true;
      this.faMicrophone = faMicrophone;
      this.onRecord();
    } else {
      this.microphoneIsActive = false;
      this.faMicrophone = faMicrophoneSlash;

      this.speechRecognitionService.destroySpeechObject();
    }
  }

  private onRecord(): void {
    this.speechRecognitionService.record().subscribe(
      value => {
        this.speechData = value;
        this.nlpService.textToCommand(value).subscribe(
          command => {
            console.log('SUCCESS: ', JSON.stringify(command));

            this.executeCommand(command);
          },
          error => {
            console.log('ERROR: ' + error);
          }
        );
      },
      error => {
        if (error.error === 'no-speech') {
          this.onRecord();
        }
      },
      () => {
        if (this.microphoneIsActive) {
          this.onRecord();
        } else {
          this.speechRecognitionService.destroySpeechObject();
        }
      }
    );
  }

  public showDetails(node: go.Node | null) {
    this.node = node;
    if (node) {
      // copy the editable properties into a separate Object
      this.data = {
        text: node.data.text,
        color: node.data.color
      };
    } else {
      this.data = null;
    }
  }

  public onCommitDetails() {
    if (this.node) {
      const model = this.node.diagram.model;
      // copy the edited properties back into the node's model data,
      // all within a transaction
      model.startTransaction();
      model.setDataProperty(this.node.data, 'text', this.data.text);
      model.setDataProperty(this.node.data, 'color', this.data.color);
      model.commitTransaction('modified properties');
    }
  }

  public onCancelChanges() {
    // wipe out anything the user may have entered
    this.showDetails(this.node);
  }

  public onModelChanged(c: go.ChangedEvent) {
    // who knows what might have changed in the selected node and data?
    this.showDetails(this.node);
  }

  public executeCommand(command: Command) {
    if (
      !(<string[]>Object.values(Action)).includes(command.action) ||
      !(<string[]>Object.values(Target)).includes(command.target)
    ) {
      return this.alertService.error(
        'Comando inválido, por favor intente nuevamente'
      );
    }

    const method = this.modelManagement.handleCommand(
      command.action,
      command.target
    );
    this.modelManagement[method](command);
  }

  public onAdd(model: Model) {
    this.modelService.add(model).subscribe(
      message => {
        this.alertService.success(message);
        this.timerSubscription ? this.timerSubscription.unsubscribe() : null;
      },
      error => {
        this.alertService.error(error);
        this.timerSubscription ? this.timerSubscription.unsubscribe() : null;
      }
    );
  }

  public onUpdate() {
    this.modelService
      .update(
        this.currentModel && this.currentModel._id ? this.currentModel._id : undefined,
        this.currentModel && this.currentModel.name ? this.currentModel.name : undefined,
        this.model.nodeDataArray,
        this.model.linkDataArray
      )
      .subscribe(
        message => {
          this.alertService.success(message);
          if (this.timerSubscription) {
            this.timerSubscription.unsubscribe();
          }
        },
        error => {
          this.alertService.error(error);
          if (this.timerSubscription) {
            this.timerSubscription.unsubscribe();
          }
        }
      );
  }

  public ngOnDestroy() {
    this.modelService.clear();

    this.microphoneIsActive = false;
    this.faMicrophone = faMicrophoneSlash;

    this.speechRecognitionService.destroySpeechObject();

    if (this.currentModelSubjectListener) {
      this.currentModelSubjectListener.unsubscribe();
    }

    if (this.modelSubjectListener) {
      this.modelSubjectListener.unsubscribe();
    }

    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }
}
