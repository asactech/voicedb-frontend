import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AuthGuard } from '../core';
import { SharedModule } from '../shared';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ModelEditorComponent } from './model-editor/model-editor.component';

@NgModule({
  declarations: [
    HomeComponent,
    ModelEditorComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    FontAwesomeModule,
    SharedModule
  ],
  providers: [
    AuthGuard
  ]
})
export class HomeModule { }
