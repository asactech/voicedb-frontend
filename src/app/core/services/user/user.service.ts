import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from './../../../../environments/environment';

import { User } from '../../../shared';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userSubject = new Subject<User>();
  private ROUTE = 'users';

  constructor(private http: HttpClient, private authService: AuthService) {}

  public getUserSubject() {
    return this.userSubject.asObservable();
  }

  public getUser(fields: string) {
    return this.http
      .get<{ message: string; user: User }>(
        `${environment.apiEndpoint}/${this.ROUTE}/${
          this.authService.currentUser.id
        }?fields=${fields}`
      )
      .pipe(
        map(data => {
          this.userSubject.next(data.user);
          if (data.user.birthDate) {
            const birthDate = new Date(data.user.birthDate);
            data.user.birthDate = {
              year: birthDate.getFullYear(),
              month: birthDate.getMonth() + 1,
              day: birthDate.getDate()
            };
          }
          return data.user;
        })
      );
  }

  public updateUser(user: User) {
    return this.http
      .patch<{ message: string; user: User }>(
        `${environment.apiEndpoint}/${this.ROUTE}/${
          this.authService.currentUser.id
        }`,
        {
          fullName: user.fullName,
          email: user.email,
          birthDate: new Date(
            user.birthDate.year +
              '-' +
              user.birthDate.month +
              '-' +
              user.birthDate.day
          ),
          occupation: user.occupation,
          companyOrEducation: user.companyOrEducation,
          country: user.country
        }
      )
      .pipe(
        map(data => {
          this.userSubject.next(data.user);
          return data.message;
        })
      );
  }

  public uploadAvatar(avatar: File) {
    const formData = new FormData();
    formData.append('avatar', avatar);

    return this.http
      .patch<{ message: string; user: User }>(
        `${environment.apiEndpoint}/${this.ROUTE}/${
          this.authService.currentUser.id
        }`,
        formData
      )
      .pipe(
        map(data => {
          this.userSubject.next(data.user);
          return data.user.avatar;
        })
      );
  }

  public changePassword(password, newPassword) {
    return this.http
      .patch<{ message: string; user: User }>(
        `${environment.apiEndpoint}/${this.ROUTE}/${
          this.authService.currentUser.id
        }`,
        {
          password: password,
          newPassword: newPassword
        }
      )
      .pipe(
        map(data => {
          this.userSubject.next(data.user);
          return data.message;
        })
      );
  }
}
