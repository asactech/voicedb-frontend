import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { User } from 'src/app/shared';

import { UserService } from './user.service';

describe('UserService', () => {
  let userService: UserService;
  let user: User = {};
  let response = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [UserService]
    });

    userService = TestBed.get(UserService);
  });

  it('should be created', () => {
    expect(userService).toBeTruthy();
  });

  it('#getUser should return a user', () => {
    response = {
      message: 'test message',
      user: {
        email: 'test@user.com',
        fullName: 'test user',
        birthDate: 'Tue, 31 Dec 2019 23:59:59 GMT',
        occupation: 'test',
        companyOrEducation: 'test',
        country: 'test',
        avatar: '../../assets/img/test.jpg'
      }
    };

    userService.getUser('').subscribe(data => {
      expect(data).toEqual(response);
    });
  });

  it('#updateUser should return a success message', () => {
    user = {
      email: 'test@user.com',
      fullName: 'test user',
      birthDate: 'Tue, 31 Dec 2019 23:59:59 GMT',
      occupation: 'test',
      companyOrEducation: 'test',
      country: 'test',
      avatar: '../../assets/img/test.jpg'
    };
    response = {
      message: 'test message'
    };

    userService.updateUser(user).subscribe(data => {
      expect(data).toEqual(response);
    });
  });

  it('#uploadAvatar should return the user new avatar', () => {
    const file: File = new File(['default_avatar'], 'src/assets/img/default_avatar.png');
    response = {
      message: 'test message'
    };

    userService.uploadAvatar(file).subscribe(data => {
      expect(data).toEqual(response);
    });
  });

  it('#changePassword should return a success message', () => {
    user = {
      password: '1234',
      confirmPassword: '1234'
    };
    response = {
      message: 'test message'
    };

    userService.changePassword(user.password, user.confirmPassword).subscribe(data => {
      expect(data).toEqual(response);
    });
  });

});
