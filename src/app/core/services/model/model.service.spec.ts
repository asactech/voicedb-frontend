import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Model } from 'src/app/shared';

import { ModelService } from './model.service';

describe('ModelService', () => {
  let modelService: ModelService;
  let model: Model = {};
  let response = [];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [ModelService]
    });

    modelService = TestBed.get(ModelService);
  });

  it('should be created', () => {
    expect(modelService).toBeTruthy();
  });

  it('#getModels should return an array of models', () => {
    response = [
      model = {
        id: 'ASD456SD4F5SD4AD',
        userId: 'ASD456SD4F5SD4AD',
        preview: '../../assets/img/default_model.jpg',
        name: 'Test Model',
        updatedModelAt: '03/03/2003',
        updatedAt: '02/02/2002',
        createdAt: '01/01/2001'
      }
    ];

    modelService.getModels('').subscribe(data => {
      expect(data).toEqual(response);
    });
  });
});
