import { Injectable } from '@angular/core';
import * as go from 'gojs';
import { ObjectData } from 'gojs';
import { Subject } from 'rxjs';
import { AlertService, Model } from 'src/app/shared';
import { Command } from 'src/app/shared/models/command.model';

export enum Action {
  CREATE = 'agregar',
  UPDATE = 'modificar',
  DELETE = 'eliminar'
}

export enum Target {
  ENTITY = 'entidad',
  ATTRIBUTE = 'atributo',
  RELATION = 'relación',
  CARDINALITY = 'cardinalidad',
  MANDATORY = 'obligatoriedad'
}

enum ArrowHead {
  LINE = 'Line',
  LFORK = 'LineFork',
  BLFORK = 'BackwardLineFork',
  CIRCLE = 'Circle',
  CFORK = 'CircleFork',
  BCFORK = 'BackwardCircleFork'
}

@Injectable({
  providedIn: 'root'
})
export class ModelManagementService {
  private arrowType = ['fromArrow', 'toArrow'];
  private modelSubject = new Subject<go.GraphLinksModel>();

  public model = new go.GraphLinksModel();

  constructor(private alertService: AlertService) {}

  public getModelSubject() {
    return this.modelSubject.asObservable();
  }

  public handleCommand(action: string, target: string) {
    switch (action) {
      case Action.CREATE:
        switch (target) {
          case Target.ENTITY:
            return 'addEntity';
          case Target.ATTRIBUTE:
            return 'addAttributes';
          case Target.RELATION:
            return 'addRelation';
        }
        break;

      case Action.UPDATE:
        switch (target) {
          case Target.ENTITY:
            return 'updateEntity';
          case Target.ATTRIBUTE:
            return 'updateAttributes';
          case Target.CARDINALITY:
            return 'updateRelation';
          case Target.MANDATORY:
            return 'updateRelation';
        }
        break;

      case Action.DELETE:
        switch (target) {
          case Target.ENTITY:
            return 'deleteEntity';
          case Target.ATTRIBUTE:
            return 'deleteAttributes';
          case Target.RELATION:
            return 'deleteRelation';
        }
        break;
    }
  }

  public addEntity(command: Command) {
    if (this.model.findNodeDataForKey(command.entities[0].key) === null) {
      this.model.addNodeData(command.entities[0]);
      this.modelSubject.next(this.model);
    } else {
      return this.alertService.error('Comando inválido, por favor intente nuevamente');
    }
  }

  public addAttributes(command: Command) {
    const entity = this.model.findNodeDataForKey(command.entities[0].key);
    if (entity === null) {
      return this.alertService.error('La entidad no existe');
    }

    const entityAttributes = entity.items.map(attribute => attribute.name);
    const attributesToAdd = command.entities[0].items.filter(
      attribute => !entityAttributes.includes(attribute.name)
    );

    this.model.set(entity, 'items', entity.items.concat(attributesToAdd));
    this.modelSubject.next(this.model);
  }

  public addRelation(command: Command) {
    const from = this.model.findNodeDataForKey(command.relations[0].from);
    const to = this.model.findNodeDataForKey(command.relations[0].to);

    if (!from && !to) {
      return this.alertService.error(
        `La entidad ${command.relations[0].from} o ${
          command.relations[0].to
        } no existe`
      );
    }

    this.model.addLinkData(command.relations[0]);
    this.modelSubject.next(this.model);
  }

  public updateEntity(command: Command) {
    const entity = this.model.findNodeDataForKey(command.entities[0].key);

    if (!entity) {
      return this.alertService.error(
        `La entidad ${command.entities[0].key} no existe`
      );
    }

    this.model.set(entity, 'key', command.entities[1].key);
    this.modelSubject.next(this.model);
  }

  public updateAttributes(command: Command) {
    const entity = this.model.findNodeDataForKey(command.entities[0].key);
    if (!entity) {
      return this.alertService.error(
        `La entidad ${command.entities[0].key} no existe`
      );
    }

    const attributesToAdd = [];
    entity.items.forEach(attribute => {
      if (attribute.name === command.entities[0].items[0].name) {
        attribute.name = command.entities[0].items[1].name;
      }
      attributesToAdd.push(attribute);
    });

    this.model.set(entity, 'items', attributesToAdd);
    this.modelSubject.next(this.model);
  }

  public updateRelation(command: Command) {
    const target = command.target;

    let exists = false;
    let index = -1;

    for (const link of this.model.linkDataArray) {
      index += 1;

      if (
        link.from === command.relations[0].from &&
        link.to === command.relations[0].to
      ) {
        exists = true;
        break;
      }
    }

    if (!exists) {
      return this.alertService.error(
        `La relación, ${command.relations[0].from}, '-', ${
          command.relations[0].to
        }, 'no existe`
      );
    }

    if (command.target === Target.CARDINALITY) {
      this.updateCardinality(command, index);
    } else {
      this.updateMandatory(command, index);
    }
  }

  private updateCardinality(command: Command, index: number) {
    const actualArrowHeads = [
      this.model.linkDataArray[index].fromArrow,
      this.model.linkDataArray[index].toArrow
    ];

    let cm_index = -1;

    for (const arrow of this.arrowType) {
      cm_index += 1;
      if (command['cardinality'][cm_index] === '1') {
        if (actualArrowHeads[cm_index] === ArrowHead.BLFORK || actualArrowHeads[cm_index] === ArrowHead.LFORK) {
          this.model.setDataProperty(
            this.model.linkDataArray[index],
            arrow,
            ArrowHead.LINE
          );
          this.modelSubject.next(this.model);
        } else if (actualArrowHeads[cm_index] === ArrowHead.BCFORK || actualArrowHeads[cm_index] === ArrowHead.CFORK) {
          this.model.setDataProperty(
            this.model.linkDataArray[index],
            arrow,
            ArrowHead.CIRCLE
          );
          this.modelSubject.next(this.model);
        }
      } else if (command['cardinality'][cm_index] === 'N') {
        if (actualArrowHeads[cm_index] === ArrowHead.LINE) {
          this.model.setDataProperty(
            this.model.linkDataArray[index],
            arrow,
            cm_index === 0 ? ArrowHead.BLFORK : ArrowHead.LFORK
          );
          this.modelSubject.next(this.model);
        } else if (actualArrowHeads[cm_index] === ArrowHead.CIRCLE) {
          this.model.setDataProperty(
            this.model.linkDataArray[index],
            arrow,
            cm_index === 0 ? ArrowHead.BCFORK : ArrowHead.CFORK
          );
          this.modelSubject.next(this.model);
        }
      }
    }
  }

  private updateMandatory(command: Command, index: number) {
    const actualArrowHeads = [
      this.model.linkDataArray[index].fromArrow,
      this.model.linkDataArray[index].toArrow
    ];

    let cm_index = -1;

    for (const arrow of this.arrowType) {
      cm_index += 1;

      if (command['mandatory'][cm_index] === '0') {
        if (actualArrowHeads[cm_index] === ArrowHead.LINE) {
          this.model.setDataProperty(
            this.model.linkDataArray[index],
            arrow,
            ArrowHead.CIRCLE
          );
          this.modelSubject.next(this.model);
        } else if (
          actualArrowHeads[cm_index] === ArrowHead.BLFORK ||
          actualArrowHeads[cm_index] === ArrowHead.LFORK
        ) {
          this.model.setDataProperty(
            this.model.linkDataArray[index],
            arrow,
            cm_index === 0 ? ArrowHead.BCFORK : ArrowHead.CFORK
          );
          this.modelSubject.next(this.model);
        }
      } else if (command['mandatory'][cm_index] === '1') {
        if (actualArrowHeads[cm_index] === ArrowHead.CIRCLE) {
          this.model.setDataProperty(
            this.model.linkDataArray[index],
            arrow,
            ArrowHead.LINE
          );
          this.modelSubject.next(this.model);
        } else if (
          actualArrowHeads[cm_index] === ArrowHead.BCFORK ||
          actualArrowHeads[cm_index] === ArrowHead.CFORK
        ) {
          this.model.setDataProperty(
            this.model.linkDataArray[index],
            arrow,
            cm_index === 0 ? ArrowHead.BLFORK : ArrowHead.LFORK
          );
          this.modelSubject.next(this.model);
        }
      }
    }
  }

  public deleteEntity(command: Command) {
    const entity = this.model.findNodeDataForKey(command.entities[0].key);
    if (!entity) {
      return this.alertService.error(
        `La entidad ${command.entities[0].key} no existe`
      );
    }

    this.model.removeNodeData(entity);
    this.modelSubject.next(this.model);
  }

  public deleteAttributes(command: Command) {
    const entity = this.model.findNodeDataForKey(command.entities[0].key);
    if (!entity) {
      return this.alertService.error(
        `La entidad ${command.entities[0].key} no existe`
      );
    }

    const attributesToDelete = command.entities[0].items.map(
      attribute => attribute.name
    );
    const entityAttributes = entity.items.filter(
      attribute => !attributesToDelete.includes(attribute.name)
    );

    this.model.set(entity, 'items', entityAttributes);
    this.modelSubject.next(this.model);
  }

  public deleteRelation(command: Command) {
    let exists = false;
    let index = -1;
    for (const link of this.model.linkDataArray) {
      index += 1;

      if (
        link.from === command.relations[0].from &&
        link.to === command.relations[0].to
      ) {
        exists = true;
        break;
      }
      ``;
    }

    if (!exists) {
      return this.alertService.error(
        `La relación, ${command.relations[0].from}, '-', ${
          command.relations[0].to
        }, 'no existe`
      );
    }

    this.model.removeLinkData(this.model.linkDataArray[index]);
    this.modelSubject.next(this.model);
  }

  public loadModel(model: Model) {
    this.model.clear();
    this.model.addNodeDataCollection(model.nodeDataArray);
    this.model.addLinkDataCollection(model.linkDataArray);

    this.modelSubject.next(this.model);
  }
}
