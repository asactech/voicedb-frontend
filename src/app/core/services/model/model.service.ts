import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ObjectData } from 'gojs';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { Model } from '../../../shared';
import { AuthService } from '../auth/auth.service';
import { environment } from './../../../../environments/environment';
import { ModelManagementService } from './model-management.service';

@Injectable({
  providedIn: "root"
})
export class ModelService {
  public currentModel: Model = {};
  public currentModelSubject = new Subject<Model>();

  private MODELS_ROUTE = "models";
  private USERS_ROUTE = "users";

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private modelManagementService: ModelManagementService
  ) {}

  public getCurrentModelSubject() {
    return this.currentModelSubject.asObservable();
  }

  public getModels(fields: string) {
    return this.http
      .get<{ models: Array<Model> }>(
        `${environment.apiEndpoint}/${this.USERS_ROUTE}/${
          this.authService.currentUser.id
        }/${this.MODELS_ROUTE}`
      )
      .pipe(
        map(data => {
          for (const model of data.models) {
            let date: string;
            moment.locale("es");

            if (model.createdAt) {
              date = moment(new Date(model.createdAt), "DD/MM/YYYY").fromNow();
              model.createdAt = date;
            }

            if (model.updatedAt) {
              date = moment(new Date(model.updatedAt), "DD/MM/YYYY").fromNow();
              model.updatedAt = date;
            }

            if (model.updatedModelAt) {
              date = moment(
                new Date(model.updatedModelAt),
                "DD/MM/YYYY"
              ).fromNow();
              model.updatedModelAt = date;
            }
          }

          return data.models;
        })
      );
  }

  public add(model: Model) {
    return this.http
      .post<{ message: string; name: string, _id: string}>(
        `${environment.apiEndpoint}/${this.USERS_ROUTE}/${
          this.authService.currentUser.id
        }/${this.MODELS_ROUTE}/`,
        {
          name: model.name,
          createdAt: new Date()
        }
      )
      .pipe(
        map(data => {
          this.currentModel._id = data._id;
          this.currentModel.name = data.name;
          this.currentModelSubject.next(this.currentModel);

          return data.message;
        })
      );
  }

  public load(modelId: string, fields: string) {
    return this.http
      .get<{ models: Model }>(
        `${environment.apiEndpoint}/${this.USERS_ROUTE}/${
          this.authService.currentUser.id
        }/${this.MODELS_ROUTE}/${modelId}?fields=${fields}`
      )
      .pipe(
        map(data => {
          this.currentModel = data.models;
          this.currentModelSubject.next(data.models);

          this.modelManagementService.loadModel(this.currentModel);

          return true;
        })
      );
  }

  public update(
    modelId?: string,
    name?: string,
    nodeDataArray?: ObjectData[],
    linkDataArray?: ObjectData[]
  ) {
    return this.http
      .patch<{ message: string; model: Model }>(
        `${environment.apiEndpoint}/${this.USERS_ROUTE}/${
          this.authService.currentUser.id
        }/${this.MODELS_ROUTE}/${modelId ? modelId : this.currentModel._id}`,
        {
          ...(name && { name: name }),
          ...(nodeDataArray && { nodeDataArray: nodeDataArray }),
          ...(linkDataArray && { linkDataArray: linkDataArray })
        }
      )
      .pipe(
        map(data => {
          return data.message;
        })
      );
  }

  public delete(modelId: string) {
    return this.http
      .delete<{ message: string }>(
        `${environment.apiEndpoint}/${this.USERS_ROUTE}/${
          this.authService.currentUser.id
        }/${this.MODELS_ROUTE}/${modelId}`
      )
      .pipe(
        map(data => {
          if (this.currentModel._id === modelId) {
            this.clear();
          }

          return data.message;
        })
      );
  }

  public clear() {
    if (this.currentModel) {
      this.currentModel = {};
      this.currentModelSubject.next();
      this.modelManagementService.model.clear();
    }
  }
}
