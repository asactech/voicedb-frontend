import { TestBed } from '@angular/core/testing';

import { ModelManagementService } from './model-management.service';

describe('ModelManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModelManagementService = TestBed.get(ModelManagementService);
    expect(service).toBeTruthy();
  });
});
