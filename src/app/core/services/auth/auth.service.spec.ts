import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { User } from '../../../shared';
import { AuthService } from './auth.service';

describe('AuthService', () => {
  let authService: AuthService;
  let user: User = {};
  let response: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [AuthService]
    });

    authService = TestBed.get(AuthService);

    const store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      }
    };

    spyOn(localStorage, 'getItem').and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem').and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'removeItem').and.callFake(mockLocalStorage.removeItem);
    spyOn(authService.router, 'navigate').and.callFake(() => {});
  });

  it('should be created', () => {
    expect(authService).toBeTruthy();
  });

  it('#signUp should return a success message', () => {
    user = {
      email: 'test@user.com',
      password: '123456'
    };
    response = {
      message: 'test message'
    };

    authService.signUp(user).subscribe(message => {
      expect(message).toEqual(response.message);
    });
  });

  it('#signIn should return access credentials for the user', () => {
    user = {
      email: 'test@user.com',
      password: '123456'
    };
    response = {
      message: 'test message',
      user: {
        id: '1234',
        token: '1234',
        expiresAt: 'Tue, 31 Dec 2019 23:59:59 GMT'
      }
    };

    authService.signIn(user, true).subscribe(data => {
      jest.spyOn(authService, 'setSession');

      expect(data).toEqual(response);
      expect(authService.isAuthenticated).toBe(true);
      expect(authService.currentUser).toEqual(response.user);
      expect(authService.setSession).toHaveBeenCalled();
    });
  });

  it('#setSession should save user data into localStorage', () => {
    user = {
      id: '1234',
      token: '1234',
      expiresAt: 'Tue, 31 Dec 2019 23:59:59 GMT'
    };

    authService.setSession(user);
    expect(localStorage.getItem('id')).toEqual(user.id);
    expect(localStorage.getItem('token')).toEqual(user.token);
    expect(localStorage.getItem('expires_at')).toEqual(user.expiresAt);
  });

  it('#isLoggedIn should set user as authenticated and save user data into the app variables', () => {
    user = {
      id: '1234',
      token: '1234',
      expiresAt: 'Tue, 31 Dec 2019 23:59:59 GMT'
    };

    authService.setSession(user);
    authService.isLoggedIn();
    expect(authService.isAuthenticated).toBe(true);
    expect(authService.currentUser.id).toEqual(user.id);
    expect(authService.currentUser.token).toEqual(user.token);
  });

  it('#signOut should delete user data from localStorage, clear app variables and set user as non authenticated', () => {
    authService.signOut();

    expect(localStorage.getItem('id')).toEqual(null);
    expect(localStorage.getItem('token')).toEqual(null);
    expect(localStorage.getItem('expires_at')).toEqual(null);

    expect(authService.currentUser).toEqual(null);
    expect(authService.isAuthenticated).toEqual(false);

    expect(authService.router.navigate).toHaveBeenCalled();
  });

  it('#passwordRecover should return a success message', () => {
    user = {
      email: 'test@user.com',
    };
    response = {
      message: 'test message'
    };

    authService.passwordRecover(user).subscribe(message => {
      expect(message).toEqual(response.message);
    });
  });

  it('#passwordReset should return a success message', () => {
    user = {
      password: '1234',
      confirmPassword: '1234'
    };
    response = {
      message: 'test message'
    };

    authService.passwordReset('1234', 'to-ken', user).subscribe(message => {
      expect(message).toEqual(response.message);
    });
  });
});
