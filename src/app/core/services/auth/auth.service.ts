import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from './../../../../environments/environment';

import { User } from '../../../shared';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public currentUser: User = {};
  public isAuthenticated = false;
  public authStatusSubject = new Subject<boolean>();

  private ROUTE = 'auth';

  private get id() {
    return localStorage.getItem('id');
  }

  private get token() {
    return localStorage.getItem('token');
  }

  private get expiration() {
    return localStorage.getItem('expires_at');
  }

  public getAuthStatusSubject() {
    return this.authStatusSubject.asObservable();
  }

  constructor(private http: HttpClient, public router: Router) {}

  public signUp(user: User) {
    return this.http
      .post<{ message: string }>(`${environment.apiEndpoint}/${this.ROUTE}/signup`, {
        email: user.email,
        password: user.password
      })
      .pipe(
        map(data => {
          return data.message;
        })
      );
  }

  public signIn(user: User, rememberMe: boolean) {
    return this.http
      .post<{
        message: string;
        user: { id: string; token: string; expiresAt: string };
      }>(`${environment.apiEndpoint}/${this.ROUTE}/login`, {
        email: user.email,
        password: user.password
      })
      .pipe(
        map(data => {
          this.currentUser = {
            ...data.user,
            expiresAt: new Date(data.user.expiresAt).toUTCString()
          };

          this.isAuthenticated = true;
          this.authStatusSubject.next(true);

          if (rememberMe) {
            this.setSession(this.currentUser);
          }

          return;
        })
      );
  }

  public setSession(user: User) {
    localStorage.setItem('id', user.id);
    localStorage.setItem('token', user.token);
    localStorage.setItem('expires_at', user.expiresAt);
  }

  public isLoggedIn() {
    const expiresAt = new Date(this.expiration);
    const now = new Date();

    if (expiresAt.getTime() > now.getTime()) {
      this.currentUser.id = this.id;
      this.currentUser.token = this.token;

      this.isAuthenticated = true;
      this.authStatusSubject.next(true);
    }
  }

  public signOut() {
    localStorage.removeItem('id');
    localStorage.removeItem('token');
    localStorage.removeItem('expires_at');

    this.currentUser = null;

    this.isAuthenticated = false;
    this.authStatusSubject.next(false);

    this.router.navigate(['/auth/sign-in']);
  }

  public passwordRecover(user: User) {
    return this.http
      .post<{ message: string }>(`${environment.apiEndpoint}/${this.ROUTE}/recovery`, {
        email: user.email
      })
      .pipe(
        map(data => {
          return data.message;
        })
      );
  }

  public passwordReset(userID: string, userToken: string, user: User) {
    return this.http
      .patch<{ message: string }>(`${environment.apiEndpoint}/${this.ROUTE}/reset`, {
        id: userID,
        token: userToken,
        password: user.password,
        confirmPassword: user.confirmPassword
      })
      .pipe(
        map(data => {
          return data.message;
        })
      );
  }
}
