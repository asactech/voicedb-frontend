import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../../environments/environment';
import { Command } from 'src/app/shared/models/command.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NlpService {
  private ROUTE = '';

  constructor(private http: HttpClient) { }

  public textToCommand(text: string) {
    return this.http
      .post<{ command: Command }>(`${environment.nlpEndpoint}/${this.ROUTE}`, {
        text: text
      })
      .pipe(
        map(data => {
          return data.command;
        })
      );
  }
}
