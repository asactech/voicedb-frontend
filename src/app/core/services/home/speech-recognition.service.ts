import { Injectable, NgZone } from '@angular/core';
import * as _ from 'lodash';
import { Observable, timer, Subscription } from 'rxjs';
import { AlertService } from 'src/app/shared';

interface IWindow extends Window {
  webkitSpeechRecognition: any;
  SpeechRecognition: any;
}

@Injectable({
  providedIn: 'root'
})
export class SpeechRecognitionService {
  public speechRecognition: any;
  public timerSubscription: Subscription;

  constructor(private zone: NgZone, private alertService: AlertService) {}

  public record(): Observable<string> {
    return Observable.create(
      (observer: {
        next: (arg0: string) => void;
        error: (arg0: any) => void;
        complete: () => void;
      }) => {
        const { webkitSpeechRecognition }: IWindow = <IWindow> window;

        this.speechRecognition = new webkitSpeechRecognition();
        this.speechRecognition.continuous = true;
        this.speechRecognition.interimResults = true;
        this.speechRecognition.lang = 'es-ve';
        this.speechRecognition.maxAlternatives = 1;

        this.speechRecognition.onresult = (speech: {
          results: { [x: string]: any };
          resultIndex: string | number;
        }) => {
          let term = '';

          if (speech.results) {
            this.timerSubscription ? this.timerSubscription.unsubscribe() : null;
            this.startTimer();
            const result = speech.results[speech.resultIndex];
            const transcript = result[0].transcript;

            if (result.isFinal) {
              this.timerSubscription.unsubscribe();
              if (result[0].confidence < 0.3) {
                this.alertService.error('Resultado no reconocido: vuelva a intentarlo');
              } else {
                term = _.trim(transcript);
                console.log('Did you said? -> ' + term + ' , If not then say something else...');
                this.zone.run(() => {
                  observer.next(term);
                });
              }
            }
          }
        };

        this.speechRecognition.onerror = (error: any) => {
          observer.error(error);
        };

        this.speechRecognition.onend = () => {
          observer.complete();
          this.timerSubscription ? this.timerSubscription.unsubscribe() : null;
        };

        this.speechRecognition.start();
      }
    );
  }

  public destroySpeechObject() {
    if (this.speechRecognition) {
      this.speechRecognition.stop();
    }
  }

  private startTimer() {
    const timerSource = timer(0, 500);
    this.timerSubscription = timerSource.subscribe( number => {
      if (2 <= number) {
        this.speechRecognition.stop();
      }
    });
  }
}
