import { TestBed } from '@angular/core/testing';
import { SpeechRecognitionService } from './speech-recognition.service';
import { O_TRUNC } from 'constants';


describe('SpeechRecognitionService', () => {
  let speechRecognitionService: SpeechRecognitionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpeechRecognitionService]
    });

    speechRecognitionService = TestBed.get(SpeechRecognitionService);
  });

  it('should be created', () => {
    expect(speechRecognitionService).toBeTruthy();
  });

  describe('#onRecord', () => {
    it('should set speech recognition continuous to true', () => {
      speechRecognitionService.record().subscribe(() => {
        jest.spyOn(speechRecognitionService.speechRecognition, 'continuous');

        expect(speechRecognitionService.speechRecognition.continuous).toHaveBeenCalled();
        expect(speechRecognitionService.speechRecognition.continuous).toBe(true);
      });
    });

    it('should set speech recognition lang to "es-ve"', () => {
      speechRecognitionService.record().subscribe(() => {
        jest.spyOn(speechRecognitionService.speechRecognition, 'lang');

        expect(speechRecognitionService.speechRecognition.lang).toHaveBeenCalled();
        expect(speechRecognitionService.speechRecognition.lang).toBe('es-ve');
      });
    });

    it('should set speech recognition maxAlternatives to "1', () => {
      speechRecognitionService.record().subscribe(() => {
        jest.spyOn(speechRecognitionService.speechRecognition, 'maxAlternatives');

        expect(speechRecognitionService.speechRecognition.maxAlternatives).toHaveBeenCalled();
        expect(speechRecognitionService.speechRecognition.maxAlternatives).toBe(1);
      });
    });

    it('should set speech recognition onresult to an object', () => {
      speechRecognitionService.record().subscribe(() => {
        jest.spyOn(speechRecognitionService.speechRecognition, 'onresult');

        expect(speechRecognitionService.speechRecognition.onresult).toHaveBeenCalled();
        expect(speechRecognitionService.speechRecognition.onresult).toBe(Object);
      });
    });

    it('should set speech recognition onerror to an error object', () => {
      speechRecognitionService.record().subscribe(() => {
        jest.spyOn(speechRecognitionService.speechRecognition, 'onerror');

        expect(speechRecognitionService.speechRecognition.onerror).toHaveBeenCalled();
        expect(speechRecognitionService.speechRecognition.onerror).toBe(Object);
      });
    });

    it('should set speech recognition onend to an object', () => {
      speechRecognitionService.record().subscribe(() => {
        jest.spyOn(speechRecognitionService.speechRecognition, 'onend');

        expect(speechRecognitionService.speechRecognition.onend).toHaveBeenCalled();
        expect(speechRecognitionService.speechRecognition.onend).toBe(Object);
      });
    });

    it('should call onstart', () => {
      speechRecognitionService.record().subscribe(() => {
        jest.spyOn(speechRecognitionService.speechRecognition, 'start');

        expect(speechRecognitionService.speechRecognition.start).toHaveBeenCalled();
      });
    });
  })

  describe('#destroySpeechObject', () => {
    it('should call stop', () => {
      speechRecognitionService.record().subscribe(() => {
        jest.spyOn(speechRecognitionService.speechRecognition, 'stop');

        expect(speechRecognitionService.speechRecognition.stop).toHaveBeenCalled();
      });
    });
  });
});
