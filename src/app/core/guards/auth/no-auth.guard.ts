import { Injectable } from '@angular/core';
import { CanActivateChild, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthService } from '../../services';

@Injectable({
  providedIn: 'root'
})
export class NoAuthGuard implements CanActivateChild {
  constructor(private authService: AuthService, private router: Router) {}

  canActivateChild(): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authService.isAuthenticated) {
      this.router.navigate(['/']);
    }

    return true;
  }
}
