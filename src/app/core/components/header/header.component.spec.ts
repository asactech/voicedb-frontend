import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AuthService } from '../../services';
import { HeaderComponent } from './header.component';
import { By } from '@angular/platform-browser';

describe('HeaderComponent', () => {
  let headerComponent: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      imports: [HttpClientModule, RouterTestingModule, FontAwesomeModule, NgbModule],
      providers: [AuthService]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(HeaderComponent);
        headerComponent = fixture.componentInstance;
      });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(headerComponent).toBeTruthy();
  });

  describe('#signOut', () => {
    let nativeElement: any;

    beforeEach(() => {
      nativeElement = fixture.debugElement.query(By.css('button[id=signOut]')).nativeElement;
    })

    it('should render button sign out', () => {
      expect(nativeElement).toBeTruthy();
    });
  });
});
