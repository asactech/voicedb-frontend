import { Component, OnInit } from "@angular/core";
import { faChalkboard, faUser } from "@fortawesome/free-solid-svg-icons";
import { NgbDropdownConfig } from "@ng-bootstrap/ng-bootstrap";

import { AuthService, ModelService } from "../../services";
import { Subscription } from "rxjs";
import { Model } from 'src/app/shared';

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
  providers: [NgbDropdownConfig]
})
export class HeaderComponent implements OnInit {
  public faUser = faUser;
  public faChalkboard = faChalkboard;

  public currentModel: Model;
  private currentModelListener: Subscription;

  constructor(
    private ngbDropdownConfig: NgbDropdownConfig,
    private authService: AuthService,
    private modelService: ModelService
  ) {
    ngbDropdownConfig.placement = "bottom-right";
    ngbDropdownConfig.autoClose = true;
  }

  public ngOnInit() {
    this.currentModelListener = this.modelService
      .getCurrentModelSubject()
      .subscribe(currentModel => {
        this.currentModel = currentModel;
      });
  }

  public onSignOut() {
    this.modelService.clear();
    this.authService.signOut();
  }

  public ngOnDestroy() {
    this.currentModelListener && this.currentModelListener.unsubscribe();
  }
}
