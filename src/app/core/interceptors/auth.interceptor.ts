import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AuthService } from '../services';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    if (this.authService.isAuthenticated) {
      request = request.clone({
        headers: request.headers.set('Authorization', `Bearer ${this.authService.currentUser.token}`)
      });
    }

    return next.handle(request);
  }
}
