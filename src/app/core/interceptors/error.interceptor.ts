import { HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, retry, timeout } from 'rxjs/operators';
import { AlertService } from 'src/app/shared';

import { AuthService } from '../services';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private alertService: AlertService,
    private authService: AuthService
  ) {}

  intercept(httpRequest: HttpRequest<any>, httpHandler: HttpHandler) {
    let errorMessage =
      'Ha ocurrido un error inesperado, verifique su conexión a internet o intentelo nuevamente.';

    return httpHandler.handle(httpRequest).pipe(
      timeout(10000),
      retry(2),
      catchError((httpErrorResponse: HttpErrorResponse) => {
        if (httpErrorResponse.error instanceof ErrorEvent) {
          errorMessage = httpErrorResponse.error.message;
        } else {
          if (httpErrorResponse.error.message) {
            errorMessage = httpErrorResponse.error.message;
          }

          if (httpErrorResponse.error.errors) {
            return throwError(httpErrorResponse.error.errors);
          }

          if (httpErrorResponse.message) {
            if (httpErrorResponse.status === 401) {
              errorMessage = 'Acceso no autorizado';

              this.authService.signOut();
            }
          }
        }

        this.alertService.error(errorMessage);

        return throwError(errorMessage);
      })
    );
  }
}
