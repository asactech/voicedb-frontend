import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HeaderComponent } from './components';
import { AuthService, ModelService, SpeechRecognitionService, UserService } from './services';

@NgModule({
  declarations: [
    HeaderComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule,
    NgbModule,
  ],
  providers: [
    AuthService,
    ModelService,
    SpeechRecognitionService,
    UserService,
  ],
  exports: [
    HeaderComponent,
  ]
})
export class CoreModule { }
