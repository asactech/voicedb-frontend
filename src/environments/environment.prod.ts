import { config } from 'src/config.prod';

export const environment = {
  production: true,
  apiEndpoint: config.URL,
  nlpEndpoint: config.nlpURL
};
