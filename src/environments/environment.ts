import { config } from 'src/config';

export const environment = {
  production: false,
  apiEndpoint: config.URL,
  nlpEndpoint: config.nlpURL
};

